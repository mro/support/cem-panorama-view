Panorama view project_readme
===

<h1>Project information</h1>

The panorama view project aims to provide a visual aid for helping to access to the BE-CEM documentation related to BIDs maintenance.

<h1>How to create the panorama view</h1>

The panorama view is created through different programs and stages:

* [Inkscape](https://inkscape.org/) is used to create the images, i.e. the slides displayed when specific points of the accelerator complex are clicked
* A code editor like [Visual Studio Code](https://code.visualstudio.com/) is used to adjust image values, directly acting on the code
* A script (https://gitlab.cern.ch/mro/common/tools/panorama-gen) is used to combine all images (slides) with information related to each slide 
* [Sozi](https://gitlab.cern.ch/mro/third-party/Sozi) is used to generate the animations, i.e. the possibility to display the different slides when specific points of the accelerator complex are clicked (fork that fix some bugs)
* [GitLab](https://gitlab.cern.ch/mro/support/cem-panorama-view) is used as project repository

<h2>Inkscape environment</h2>

<h3>Select the main SVG file</h3>

1. Choose the SVG file which will regroup all different SVG files (slides to be displayed)

<h3>Create SVG files (slides) to be integrated into the main SVG file</h3>

1. Open Inkscape
2. Use the fonctionalities "Create end edit text objects" and "Create rectangles and square" to add shapes and text
 ![](https://codimd.web.cern.ch/uploads/upload_86c4f7d60385ea31342f97fc400ef808.png)
 3. Right-click on the created shape and click on "Fill and Stroke" to customize text and form colors

![](https://codimd.web.cern.ch/uploads/upload_11013d76637f5468e77dfd1954b97479.png)

4. To add any web external link to the text: select the text to be linked, right-click and click on “create link”
5.	In the right displayed tab, copy the link into href under Objects attributs tab
6. Click enter
7. Once all modifications are done, click on "File > Save as" to save your file


<h3>Integrate the different SVG files (slides) into the main SVG file</h3>

1. Open the main SVG file in Inkscape
2. Create a new layer, e.g. "rectanglesToBeclicked", which will be used to contain all shapes/boxes you are going to create hereafter
3. Click on "Create rectangles and square" to add a shape exactly where you want to display the SVG file to be integrated
4. Right-click on the new shape and move to the layer previously created, i.e. "rectanglesToBeclicked"
5. Click on the Edit main menu and then XML Editor to select the new shape and to finally modify its ID and xlink:href.
* As xlink:href, type # and the name of the SVG file you want to display by clicking on the new shape
* As ID type the name the SVG file you want to display by clicking on the new shape, followed by Rectangle. By doing so, you are able to understand which SVG file the rectangle will point.

![](https://codimd.web.cern.ch/uploads/upload_9ad1c2f6d1bb2228c2af2a1076c84442.png)

6.  Right-click on the new shape and click on "Fill and Stroke" to customize the form colors
7.  Open the folder containing the SVG file to be integrated and drag and drop it into the main SVG file. Select the third option when you import the SVG file

![](https://codimd.web.cern.ch/uploads/upload_373d6547da7588c6a786e09a42ef0186.png)
8. Hold down the ctrl key and the mouse click to resize the SVG file and to ensure its ratio
9. Place the SVG file exactly where you want to display it

![](https://codimd.web.cern.ch/uploads/upload_637acdbb12c156ad4be7d88fc66d6bbc.png)

10.	Create a new layer renamed as follows: nameOfSVGFileIntegratedLayer (e.g. HiradMatLayer).
11.	Select on the XML Editor the new layer and modify its ID and label 
![](https://codimd.web.cern.ch/uploads/upload_3e55ec719324e91ce736c606287900bc.png)
12. Right-click on the SVG integrated file and click on "Move to Layer": select the corresponding layer that you previously created


![](https://codimd.web.cern.ch/uploads/upload_a82222b700b3a49262949bfb2dca2573.png)

13.	On the XML Editor modify the ID of the SVG integrated file and check its xlink:href (it is important that it points to the path of the SVG integrated file). 

![](https://codimd.web.cern.ch/uploads/upload_181d10b3a8a0457501aaa637bfc78265.png)



14. Repeat the same procedure for all SVG files to be integrated into the main SVG
15. Save the main SVG file as .svg

<h2>Adjust values in Visual Studio Code</h2>

1. Right-click on each SVG file that was integreted into the main SVG and open it with Visual Studio Code
2. Adjust the width, the height and the viewBox values according to the printscreen hereafter (only SPS, ISOLDE and EA have different values, i.e. viewBox= 0 0 210 367 because of the higher number of rows)

![](https://codimd.web.cern.ch/uploads/upload_da424d8b1c0a27ff8b018b818ddfe0cb.png)

3. Add the following attribute "transform=translate" with the correspoding value (all SVG)

![](https://codimd.web.cern.ch/uploads/upload_d4e138ced0cbfeae11bfc7600cb94397.png)

4. Some SVG files, e.g. SPS, ISOLDE and EA, have different viewBox values, but also the additional attribute tranform=translate at the level of the arrow, which is placed inside a new "<g>"

![](https://codimd.web.cern.ch/uploads/upload_a95e6d1beab2d6a558ad5b4487edbc82.png)


<h2>Run and execute the script</h2>

1. Click on the file 'watch.bat', containing the script which will generate the output file from the file "acceleratorComplex.svg" 

![](https://codimd.web.cern.ch/uploads/upload_b53db61ebf9412f4076e44a0af93f417.png)

![](https://codimd.web.cern.ch/uploads/upload_ed56ee28d856f61a723904155ddb3eda.png)

![](https://codimd.web.cern.ch/uploads/upload_bc22bd67c44cd813a4a4e68e361a4872.png)
 

<h2>Create presentations in SOZI</h2>

1.	Open Sozi and select the main SVG file

![](https://codimd.web.cern.ch/uploads/upload_50e0563cd5e1d3b2aa42c2cddb69bc12.png)

2.	Click on Add all layers

![](https://codimd.web.cern.ch/uploads/upload_e03fa0bf3caae0b1596fe0a8660dc78c.png)

3. Create a new frame by clicking on "+"

![](https://codimd.web.cern.ch/uploads/upload_0ad82aed4bb7c9ce204ce8f544a674af.png)

4. Click on the new frame and modify its Title and ID: copy the ID used in Inskape for the corresponding SVG integrated file and paste it into Title and ID fields

![](https://codimd.web.cern.ch/uploads/upload_9c8cb237d0c2c2ca9b39130fb7242a4b.png)

5. Adjust the layer opacity (the frame should be fully displayed in its corresponding layer and it shoulb be hidden ("transparent") in the others 

![](https://codimd.web.cern.ch/uploads/upload_b5768341beef1e29a172b82857724d4e.png)

6.	Place the cursor on the frame preview and by holding down the left mouse button and ctrl, resize it and place it at the center

![](https://codimd.web.cern.ch/uploads/upload_3249d835303c5aa64b63fb63d8b38b15.png)

7.	Deselect all options hereafter to not display back button, frame number and list. Select instead the option to hide the path number (crossed out eye).

![](https://codimd.web.cern.ch/uploads/upload_a773e174d7234424fe43cf23ec510559.png)


![](https://codimd.web.cern.ch/uploads/upload_d08e91faa11c0c253480c8c08aa49027.png)


8. Repeat the same procedure for all SVG file previously integrated in Inkscape
8.	Save the presentation (Ctrl+S) and close SOZI
    
<h2>Open the final .svg file with animations</h2>
    
1.	Double-click on the file "acceleratorDouble-complex.sozi.html" in the output folder to access to the final file with the animations

![](https://codimd.web.cern.ch/uploads/upload_25aa41fbe3a1020961a6156537577236.png)

![](https://codimd.web.cern.ch/uploads/upload_9643334f905cfaeb41045a8d6935faf1.png)
    
<h2>Upload the panorama project via GitLab</h2>
    
1. Open Visual Studio Code and make all necessary modifications in the files containing the slides (e.g. AD, NA, etc.)
2. Run the script and then open Sozi to upload the presentation with the last changes
    
![](https://codimd.web.cern.ch/uploads/upload_d2e93e72fe7a929fc21826f41bd371d1.png)
    
![](https://codimd.web.cern.ch/uploads/upload_0e07f2b2e2f8e1db8604b5baba5f54de.png)


2. Now the presentation is updated with last changes.In Visual Studio Code, click on "Terminal > New terminal"
3. Type the command "git status" to see all modifications done
    
![](https://codimd.web.cern.ch/uploads/upload_8d4ea083645cdc3b8b2112964cfaf39f.png)
    
4. Verify that you are in the "project head", and then to include all modifications/updates in the next commit, type the command "git add ."
All previous modifications will have now the green color.

![](https://codimd.web.cern.ch/uploads/upload_ec273ed5b5bf723efa4709c7cc0c88bc.png)

    
5. To store all modifications added, type the command "git commit -m 'Write a comment which summarizes your modifications/updates'
    
![](https://codimd.web.cern.ch/uploads/upload_2cabb6c86d1d897894dac354eb957457.png)

6. To update upload the local repository updates to the remote repository, type the command "git push"
    
![](https://codimd.web.cern.ch/uploads/upload_28f6f6091a8eb5cb5b4bdd046c1fafc2.png)

7. Go on https://gitlab.cern.ch/mro/support/cem-panorama-view and click on "CI/CD > Pipelines"

![](https://codimd.web.cern.ch/uploads/upload_8028cb1710a0f4d3339d0b29dfc7760c.png)
    
8. Click on the icon below to run and execute the last updates which will automatically be visible in "https://mro-dev.web.cern.ch/ci/mro/support/cem-panorama-view/acceleratorComplex.sozi.html", where you do the final check.

![](https://codimd.web.cern.ch/uploads/upload_ffae71c88d91e83feb150249215878b3.png)
    
![](https://codimd.web.cern.ch/uploads/upload_d98ece9dc6336393282f10078cc3e848.png)




